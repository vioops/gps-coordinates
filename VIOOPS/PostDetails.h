//
//  PostDetails.h
//  VIOOPS
//
//  Created by Purnendu mishra on 25/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "VIGlobalViewController.h"

@interface PostDetails : VIGlobalViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblPostComments;

@end
