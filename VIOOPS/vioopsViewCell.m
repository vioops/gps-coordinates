//
//  vioopsViewCell.m
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "vioopsViewCell.h"

@implementation vioopsViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.imgCell = [[UIImageView alloc]init];
        self.imgCell.frame = CGRectMake(0, 0, frame.size.width, frame.size.height-100);
        self.imgCell.image=[UIImage imageNamed:@"img1"];
        
       
        self.ImgUser=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        self.ImgUser.center=CGPointMake(25, CGRectGetMaxY(self.imgCell.frame)+28);
        self.ImgUser.layer.cornerRadius=CGRectGetWidth(self.ImgUser.frame)/2.0f;
        self.ImgUser.layer.masksToBounds=YES;
        
        //self.ImgUser.backgroundColor=[UIColor grayColor];
        self.ImgUser.image=[UIImage imageNamed:@"usrImg"];
        
        [self addSubview:self.ImgUser];
        
        
        
        self.lblUserName=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width-CGRectGetMaxX(self.ImgUser.frame), 17)];
        self.lblUserName.center=CGPointMake(CGRectGetMaxX(self.ImgUser.frame)+CGRectGetWidth(_lblUserName.frame)/2.0f+5, CGRectGetMinY(_ImgUser.frame)+CGRectGetHeight(_lblUserName.frame)/2.0f +3);
        _lblUserName.textColor=[UIColor lightGrayColor];
        
        _lblUserName.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
        
       // self.lblUserName.backgroundColor=[UIColor purpleColor];
        [self addSubview:self.lblUserName];
        
        self.lblLocation=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width-CGRectGetMaxX(self.ImgUser.frame), 14)];
        self.lblLocation.center=CGPointMake(CGRectGetMaxX(self.ImgUser.frame)+CGRectGetWidth(_lblUserName.frame)/2.0f+5, CGRectGetMaxY(_lblUserName.frame)+CGRectGetHeight(_lblLocation.frame)/2.0f +3);
          _lblLocation.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
         _lblLocation.textColor=[UIColor lightGrayColor];
       // self.lblLocation.backgroundColor=[UIColor purpleColor];
        [self addSubview:self.lblLocation];
        
        
        
        UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 1)];
        lblLine.center=CGPointMake(lblLine.center.x, CGRectGetMaxY(_ImgUser.frame)+10);
        
        lblLine.backgroundColor=[UIColor lightGrayColor];
        
        
        UIImageView *imgScoreIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        imgScoreIcon.center=CGPointMake(15, CGRectGetMaxY(lblLine.frame)+17);
        //imgScoreIcon.backgroundColor=[UIColor grayColor];
        imgScoreIcon.image=[UIImage imageNamed:@"cupImg"];
        
        [self addSubview:imgScoreIcon];
        
        _lblScore=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 17)];
        _lblScore.center=CGPointMake(CGRectGetMaxX(imgScoreIcon.frame)+56, CGRectGetMinY(imgScoreIcon.frame)+10);
        _lblScore.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
        _lblScore.text=@"Score 50";
        _lblScore.textColor=[UIColor orangeColor];
        
        
        [self addSubview:_lblScore];
        
        
        UIButton *btnUp=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        btnUp.center=CGPointMake(frame.size.width-15, CGRectGetMaxY(lblLine.frame)+18);
        //btnUp.backgroundColor=[UIColor blackColor];
        [btnUp setBackgroundImage:[UIImage imageNamed:@"arrowUpp"] forState:UIControlStateNormal];
        
        [self addSubview:btnUp];
        
        UIButton *btnDown=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        btnDown.center=CGPointMake(CGRectGetMinX(btnUp.frame)-15, CGRectGetMaxY(lblLine.frame)+18);
       // btnDown.backgroundColor=[UIColor blackColor];
        [btnDown setBackgroundImage:[UIImage imageNamed:@"arrowDown"] forState:UIControlStateNormal];
        [self addSubview:btnDown];
        
        
        
        self.backgroundColor=[UIColor colorWithRed:246/255.0f green:246/255.0f blue:246/255.0f alpha:1];
        
        [self addSubview:lblLine];
        
        [self addSubview:self.imgCell];
        
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 8.0f;
    }
    return self;
}

@end
