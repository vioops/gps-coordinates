//
//  KHMainHeader.h
//  khadamati
//
//  Created by Souvik on 22/05/15.
//  Copyright (c) 2015 karmick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
//static id weakObject(id object)
//{
//    __block typeof(object) weakSelf = object;
//    return weakSelf;
//}
#ifndef khadamati_KHMainHeader_h
#define khadamati_KHMainHeader_h
#define MainStory [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define iPhoneHeight  [[UIScreen mainScreen] bounds].size.height
#define RobotoConso @"RobotoCondensed-Regular"
#define Roboto @"Roboto-Regular"
//Color code for
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
//#define API @"http://192.168.1.6/khadamati/api/"
#define API @"http://360versions.com/khadamati/api/"
#define DOMENURL @"http://360versions.com/khadamati/"
#define CHECKLOGIN @"check_login"
#define USERPROFILE @"profiledetails"
#define GOOGLEPLACEAUTOCOMPLET @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
#define GOOGLEAPIKEY @"AIzaSyBwGdUirJjPi-pRVbLpgpmjMB3-IpbJ4nk"
#define REGISTRATION @"registration"
#define SERVICE_PROVIDER @"service_provider_list"
#define HIRED_LIST @"hired_list"
#define SERVICEPROVIDERSEARCH @"search_service_provider"
#define FORGOEPASS @"forgot_password"
#define LATESTMESSAGE @"LatestMessage"
#define SHOWUSERINMAP @"show_in_map"
#define SERVICEDETAILS @"service_details"
#define REVIEW_LIST @"review_list"
#define MYREVIEWLIST @"my_review_list"
#if DEBUG == 0
#define DebugLog(...)
#elif DEBUG == 1
#define DebugLog(...) NSLog(__VA_ARGS__)
#endif

#endif
typedef void(^KHJSondataBlock) (NSURLResponse *response, NSError *error,id resopncedata);
typedef void (^KHDownloadImage)(NSError *error, UIImage *image);