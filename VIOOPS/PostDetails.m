//
//  PostDetails.m
//  VIOOPS
//
//  Created by Purnendu mishra on 25/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "PostDetails.h"
#import "PostCommentCell.h"

@interface PostDetails ()

@end

@implementation PostDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellID=@"cellID";
    
    PostCommentCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil)
    {
        
        cell=[[PostCommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.imgFriend.image=[UIImage imageNamed:@"user_img"];
    
    cell.lblTitle.text=@"Demo title text";
    cell.lblSubTitle.text=@"Demo subtitle Text comment ";
    
    
    return cell;
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
