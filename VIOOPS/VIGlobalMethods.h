//
//  VIGlobalMethods.h
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface VIGlobalMethods : NSObject
+(instancetype)Shareinstance;
-(BOOL)IsBlank:(NSString *)str;
-(BOOL)IsContainAnythingIn:(NSString *)str except:(NSString *)exceptString;
-(NSString *) Encoder:(NSString *)str;
-(NSData *)ConvertImageToNSData:(UIImage *)image;
-(NSURL *)ConvertStringToNSUrl:(NSString *)String;
-(NSString *)ConvertNSDataToNSString:(NSData *)data;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
- (BOOL)validateEmailWithString:(NSString*)email;
-(NSString *)trim:(NSString *)str;
- (BOOL)validatePhone:(NSString *)phoneNumber;
@end
