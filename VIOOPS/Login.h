//
//  Login.h
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "VIMainHeader.h"

@interface Login : VIGlobalViewController<UITextFieldDelegate>
- (IBAction)btnLoginTapped:(id)sender;

@end
