//
//  VIAFNetwork.m
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//


#import "VIMainHeader.h"
VIJsondataBlock datablock;
@implementation VIAFNetwork
//this method return single tone instance of this class
+(instancetype)ShareVIAFNetworkinstance
{
    static VIAFNetwork *GlobalCall;
    static dispatch_once_t dispatchonst;
    dispatch_once(&dispatchonst, ^{
        GlobalCall=[[VIAFNetwork alloc] init];
    });
    return GlobalCall;
}



//This method help to send data in post and also get post responce in a single method using VIJsondataBlock. Afnetwork send the data to server and return the responce via VIJsondataBlock, dataDic is post key value pare and urlstring is the main url of post method
-(void)SenddataDictionary:(NSMutableDictionary *)dataDic WithMainurl:(NSString *)UrlString Withresponce:(VIJsondataBlock)Success
{
    datablock=[Success copy];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:UrlString]];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@""
                                                      parameters:dataDic];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        
        
        id response = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        datablock(nil,nil,response);
        
        
    }
      failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DebugLog(@"Error: %@", error.localizedDescription);
         datablock(nil,error,nil);
     }];
    
    [operation start];
    
}

//As afnetwoking has very fast downloding capacity we just send the imageview and image url in this method and that image is download and set to that particular image view (imageView).
-(void)DownloadAndSetimageTo:(UIImageView *)imageView WithStringUrl:(NSString *)Stringurl
{
    
    UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activity setFrame:CGRectMake(imageView.frame.size.width/2.0f-activity.frame.size.width/2, imageView.frame.size.height/2.0f-activity.frame.size.height/2.0f, activity.frame.size.width, activity.frame.size.height)];
    [imageView addSubview:activity];
    [activity startAnimating];
    
    NSURL *Url=[NSURL URLWithString:Stringurl];
    NSURLRequest *request=[NSURLRequest requestWithURL:Url];
    AFImageRequestOperation *requestnew=[AFImageRequestOperation imageRequestOperationWithRequest:request imageProcessingBlock:nil success:^(NSURLRequest *newre, NSHTTPURLResponse *responce, UIImage *image)
                                         {
                                             [activity stopAnimating];
                                             [imageView setImage:image];
                                         }
                                                                                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
                                         {
                                             [activity stopAnimating];
                                             DebugLog(@"The error name:%@",error.description);
                                         }];
    [requestnew start];
}

//In case of uploding image to server this method is realy fine. We just send the arry of images(arrayChosenImages) and main uplod url (URLString) 
-(void)UploadmultipleimageToServerusingAFNetwork:(NSString *)URLString WithImageArry:(NSMutableArray *)arrayChosenImages WithCompletation:(VIJsondataBlock)complet
{
    datablock=[complet copy];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:URLString]];
    client.parameterEncoding = AFJSONParameterEncoding;
    
    NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:@"Mindex/getimg" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        // arrayChosenImages is NSArray of UIImage to be uploaded
        for (int i=0; i<[arrayChosenImages count]; i++)
        {
            [formData appendPartWithFileData:UIImageJPEGRepresentation([arrayChosenImages objectAtIndex:i], 0.5)
                                        name:[NSString stringWithFormat:@"image%d",i+1]
                                    fileName:[NSString stringWithFormat:@"image%d.jpg",i+1]
                                    mimeType:@"image/jpeg"];
        }
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [client enqueueHTTPRequestOperation:operation];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSData *dataResponseJSON = [operation.responseString dataUsingEncoding:NSUTF8StringEncoding];
         id dictResponseJSON = [NSJSONSerialization JSONObjectWithData:dataResponseJSON options:NSJSONReadingMutableContainers error:nil];
         datablock(nil,nil,dictResponseJSON);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         datablock(nil,error,nil);
         
     }];
    [operation start];
}

@end
