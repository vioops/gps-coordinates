//
//  VIMainHeader.h
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//
//Please do not change any content of this header file it will harm all the projects
//The purpose of this call is deduce developer effort and help to make less error
#ifndef VIOOPS_VIMainHeader_h
#define VIOOPS_VIMainHeader_h
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Friends.h"
#import "FriendsViewCell.h"
#import "SWRevealViewController.h"
#import "Home.h"
#import "Menu.h"
#import "SWRevealViewController.h"
#import "SignUp.h"
#import "ViewController.h"
#import "VIGlobalMethods.h"
#import "AppDelegate.h"
#import "VIGlobalViewController.h"
#import "AFNetworking.h"
#import "VIAFNetwork.h"
#import "AFHTTPClient.h"
#import "VIBlocksHeader.h"
#import "vioopsViewCell.h"
#import "VIFriendsViewController.h"
#define SCREEN_WIDTH        [[UIScreen mainScreen] bounds].size.width
#define MainStory [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define iPhoneHeight  [[UIScreen mainScreen] bounds].size.height
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
//Use debug log method instade of nslog becuse debuglog only effect in debuging time when you build an ipa those are automaticaly remove if you use nslog it will take memory after creating the ipa.
#if DEBUG == 0
#define DebugLog(...)
#elif DEBUG == 1
#define DebugLog(...) NSLog(__VA_ARGS__)
#endif
#endif

