//
//  VIGlobalViewController.h
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VIGlobalViewController : UIViewController
@property (nonatomic, strong) UIViewController *PresentViewController;
-(BOOL)IsCurrentViewController:(NSString *)controller;
-(void) PerformGoBackTo:(NSString *)HereWeGo WithAnimation:(BOOL)Animation;
-(void)PerformGoBack;
-(void)PerformGoBackWithNoOfTimes:(int)Times;
-(void)PerformGoBackWithTransitationFrom:(NSString *)TransitationDirection;
-(void) PerformGoBackTo:(NSString *)HereWeGo;
-(int)getStatckIndex:(NSString *)Object;
-(void)PopViewController:(NSString *)viewControllerName WithAnimation:(NSString *)AnimationType;
-(void)PushViewController:(UIViewController *)viewController WithAnimation:(NSString *)AnimationType;
-(void)PushViewController:(UIViewController *)viewController TransitationFrom:(NSString *)TransitationDirection;
+(VIGlobalViewController *)sharedViewController;
-(NSString *)LocalTimeZoneName;
-(NSString *)LocalDate;
-(void)SetBorderToTextFieldView:(UIView *)TxtFieldView Withcolor:(UIColor *)Bordercolour;
-(void)SetBorderToView:(UIView *)Views;
-(void)SetBorderToTextField:(UITextField *)TxtField Withcolor:(UIColor *)Bordercolour;
-(void)SetCornerToButton:(UIButton *)Button;
-(void)RoundMyProfilePicture:(UIImageView *)PicView;
-(void)RoundMyProfilePicture:(UIImageView *)PicView withRadious:(float)Radious;
-(void)makeRoundWithDropShadow:(UIImageView *)PicView InView:(UIView *)BackView;
-(void)setRoundBorderToImageView:(UIImageView *)PicView;
-(void)SetroundborderWithborderWidth:(CGFloat)BorderWidth WithColour:(UIColor *)RGB ForImageview:(UIImageView *)ImageView;
-(void)SetRoundborderToView:(UIView *)Viewname Withcolor:(UIColor *)ColorName WithBorderwith:(CGFloat)boderWidth;
-(void)setRoundBorderToUiview:(UIView *)uiview;
-(void)setRoundBorderTolable:(UILabel *)uiviewLabl;
-(void)setBorderToView:(UIView *)VictimView withCornerRadius:(float)Radious withColor:(UIColor *)color;
-(void)setBorderToView:(UIView *)VictimView withCornerRadius:(float)Radious withColor:(UIColor *)color withBorderWidth:(float)BorderWidth;
-(void)setBorderToViews:(NSArray *)ViewArray withCornerRadius:(float)Radious withColor:(UIColor *)color;
-(void)SetCornerToView:(UIView *)Views;
-(void)SetCornerToView:(UIView *)Views to:(float)corner;
-(void)ResignalltextfieldinaView:(UIScrollView *)Scroll;
-(void)ResignallinaUiviewsunview:(UIView *)Scroll;
-(void)SetBorderToBackView:(UIView *)BackView Withcolor:(UIColor *)Bordercolour;
-(void)SetDropShadowToBackView:(UIView *)BackView;
-(void)SetBorderToFieldIn:(UIView *)FieldView Withcolor:(UIColor *)Color;
-(void)SetDelegetsToTextFiledIn:(UIView *)FieldView;
-(void)SetBorderToSubViewOf:(UIView *)Views Withcolor:(UIColor *)Color;
-(void)SetfonToallthetextfielsUnderView:(UIView *)ViewSuper;
-(NSMutableDictionary *)GetkeyandvalueDictionary:(NSArray *)keys values:(NSArray *)Values;
-(NSString *)CreatepostString:(NSArray *)Key Withvalues:(NSArray *)values;
-(NSString *)getloginid;
-(NSString *)Getusername;
-(NSString *)GetPassword;
-(void)Setuserid:(NSString *)userid;
-(void)Setusernsme:(NSString *)username;
-(void)Setuserpassword:(NSString *)userpassord;
-(void)Setsenderimage:(NSString *)Senderimage;
-(NSString *)Getsenderimage;
-(void)UserFullname:(NSString *)username;
-(NSString *)getusernameset;
-(NSString *)GetUserType;
-(void)Setusertype:(NSString *)usertype;
-(void)ShowunexpectedError;

@end
