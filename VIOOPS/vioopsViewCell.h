//
//  vioopsViewCell.h
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface vioopsViewCell : UICollectionViewCell
@property (strong, nonatomic) UIImageView *imgCell;
@property(strong,nonatomic)UIImageView *ImgUser;
@property(strong,nonatomic)UILabel *lblUserName;
@property(strong,nonatomic)UILabel *lblLocation;
@property(strong,nonatomic)UILabel *lblScore;




@end
