//
//  FriendsViewCell.h
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewCell : UITableViewCell

@property(retain,nonatomic)UIImageView *imgFriend;
@property(retain,nonatomic) UILabel *lblTitle;
@property(retain,nonatomic) UILabel *lblSubTitle;
@property(retain,nonatomic)UIButton *btnAccept,*btnDecline;

@end
