//
//  Home.m
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "VIMainHeader.h"
#import <MapKit/MapKit.h>

@interface Home ()<UIGestureRecognizerDelegate,MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *Collectioncontanorview;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *DragableView;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation Home

- (void)viewDidLoad
{
  
    [super viewDidLoad];
    SWRevealViewController *revealController = [self revealViewController];
    CGRect framecollectionview=[_collectionGalley frame];
    framecollectionview.size.height=_Collectioncontanorview.frame.size.height-_DragableView.frame.size.height;
    [_collectionGalley setFrame:framecollectionview];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
     _mapView.delegate = self;
    [_btnSlideMenu addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [_btnListMap addTarget:self action:@selector(UpDownscrollcollection:) forControlEvents:UIControlEventTouchUpInside];
    [self.collectionGalley registerClass:[vioopsViewCell class] forCellWithReuseIdentifier:@"vioopsCell"];
        _mapView.showsUserLocation = YES;
   //Adding pangesture on dragableview for draging
    UIPanGestureRecognizer *recognizer=[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(PanUptoshowenlarge:)];
    recognizer.delegate=self;
    [_DragableView addGestureRecognizer:recognizer];
    
//Adding location on mapview
    self.locationManager = [[CLLocationManager alloc]init];
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];

    _mapView.mapType = MKMapTypeStandard;
  
    
}



//implement map region for ios
- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}


//function for updown map and list of result
-(IBAction)UpDownscrollcollection:(id)sender
{
    NSInteger maprecentheight=_mapView.frame.size.height;
    NSInteger Collectionframe=_Collectioncontanorview.frame.size.height;
    CGRect containerview = _Collectioncontanorview.frame;
    CGRect collectionview = _collectionGalley.frame;
    CGRect mapviewframe=_mapView.frame;
    
    if (maprecentheight>Collectionframe)
    {
        containerview.origin.y = 64;
        containerview.size.height=[UIScreen mainScreen].bounds.size.height-64;
        mapviewframe.size.height = 0;
        collectionview.size.height=[UIScreen mainScreen].bounds.size.height-(64.0f+_DragableView.frame.size.height);
    }
    else
    {
        containerview.origin.y = [UIScreen mainScreen].bounds.size.height-_DragableView.frame.size.height;
        containerview.size.height=_DragableView.frame.size.height;
        mapviewframe.size.height=[UIScreen mainScreen].bounds.size.height-(_DragableView.frame.size.height+64.0f);
        collectionview.size.height=0;
    }
    
    [UIView animateWithDuration:.2 animations:^{
        [_Collectioncontanorview setFrame:containerview];
        [_collectionGalley setFrame:collectionview];
        [_mapView setFrame:mapviewframe];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 10;
    
    
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    vioopsViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"vioopsCell" forIndexPath:indexPath];
  // cell.backgroundColor=[UIColor greenColor];
   // cell.imgCell.backgroundColor=[UIColor redColor];
    cell.lblUserName.text=@"Vioops User1";
    cell.lblLocation.text=@"Vioops Location";
    
    
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *searchTerm = self.searches[indexPath.section]; FlickrPhoto *photo =
//    self.searchResults[searchTerm][indexPath.row];
//    // 2
    CGSize retval = CGSizeMake(CGRectGetWidth(self.view.frame)/2.0f-10, 260);
    //retval.height += 35; retval.width += 35;

    
    return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 5, 10, 5);
   
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
    
}

//UIPangesture recognizer mathod used to calculate the chage of subviews while translation of dragable view to y direction 
-(void)PanUptoshowenlarge:(UIPanGestureRecognizer *)panRecognizer
{
    
    CGPoint translation = [panRecognizer translationInView:panRecognizer.view];
    if (panRecognizer.state==UIGestureRecognizerStateChanged)
    {
        CGRect frame = _Collectioncontanorview.frame;
        CGRect framenew = _mapView.frame;
        CGRect framenewtable = _collectionGalley.frame;
        frame.origin.y += translation.y;
        frame.size.height-=translation.y;
        framenew.size.height+=translation.y;
        framenewtable.size.height-=translation.y;
        if (frame.origin.y < 64)
        {
            frame.origin.y = 64;
            frame.size.height=[UIScreen mainScreen].bounds.size.height-64.0f;
            framenew.size.height = 0;
            framenewtable.size.height=[UIScreen mainScreen].bounds.size.height-(64.0f+_DragableView.frame.size.height);
        }
        else if (frame.origin.y>([UIScreen mainScreen].bounds.size.height-_DragableView.frame.size.height))
        {
            frame.origin.y = [UIScreen mainScreen].bounds.size.height-_DragableView.frame.size.height;
            frame.size.height=_DragableView.frame.size.height;
            framenew.size.height=[UIScreen mainScreen].bounds.size.height-(_DragableView.frame.size.height+64.0f);
            framenewtable.size.height=0;
        }
      
         [_collectionGalley setFrame:framenewtable];
        [_Collectioncontanorview setFrame:frame];
        [_mapView setFrame:framenew];
       
    }
    
    [panRecognizer setTranslation:CGPointZero inView:panRecognizer.view];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}


@end
