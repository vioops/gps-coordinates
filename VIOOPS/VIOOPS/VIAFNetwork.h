//
//  VIAFNetwork.h
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VIBlocksHeader.h"

@interface VIAFNetwork : NSObject
+(instancetype)ShareVIAFNetworkinstance;
-(void)SenddataDictionary:(NSMutableDictionary *)dataDic WithMainurl:(NSString *)UrlString Withresponce:(VIJsondataBlock)Success;
-(void)DownloadAndSetimageTo:(UIImageView *)imageView WithStringUrl:(NSString *)Stringurl;
-(void)UploadmultipleimageToServerusingAFNetwork:(NSString *)URLString WithImageArry:(NSMutableArray *)arrayChosenImages WithCompletation:(VIJsondataBlock)complet;
@end
