//
//  PostCommentCell.h
//  VIOOPS
//
//  Created by Purnendu mishra on 25/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCommentCell : UITableViewCell
@property(retain,nonatomic)UIImageView *imgFriend;
@property(retain,nonatomic) UILabel *lblTitle;
@property(retain,nonatomic) UILabel *lblSubTitle;

@end
