//
//  VIGlobalViewController.m
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "VIGlobalViewController.h"
#import "AppDelegate.h"
@interface VIGlobalViewController ()<UITextFieldDelegate>

@end

@implementation VIGlobalViewController
@synthesize PresentViewController;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
//Return true if app showing this viewcontroller

-(BOOL)IsCurrentViewController:(NSString *)controller
{
    NSInteger count=[[[self navigationController] viewControllers] count];
    
    if([controller isEqualToString:NSStringFromClass([[[[self navigationController] viewControllers] objectAtIndex:(count -1)] class])])
    {
        return TRUE;
    }
    return FALSE;
}
//Pop top spacific Methods

-(void) PerformGoBackTo:(NSString *)HereWeGo WithAnimation:(BOOL)Animation
{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    int index=0;
    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[arr count] ; i++)
    {
        if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(HereWeGo)])
        {
            index = i;
        }
    }
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToViewController:[arr objectAtIndex:index] animated:NO];
}

//This method is called when one time pop needs

-(void)PerformGoBack
{
    [self PerformGoBackWithNoOfTimes:1];
}
//this method call when you need to go back a numbor of page

-(void)PerformGoBackWithNoOfTimes:(int)Times
{
    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    NSInteger index=[arr count]-(1+Times);
    if(PresentViewController)
    {
        CATransition *Transition=[CATransition animation];
        [Transition setDuration:0.4f];
        [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [Transition setType:kCATransitionMoveIn];
        [Transition setSubtype:kCATransitionFromBottom];
        [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
        [self.navigationController popToViewController:[arr objectAtIndex:index] animated:NO];
    }
    else
        [self.navigationController popToViewController:[arr objectAtIndex:index] animated:YES];
}

//This method is called when you need animation at the time of pop

-(void)PerformGoBackWithTransitationFrom:(NSString *)TransitationDirection
{
    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    NSInteger index=[arr count]-2;
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:TransitationDirection];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
    [self.navigationController popToViewController:[arr objectAtIndex:index] animated:NO];
}

//this method takes view controller name as a string and pop to that particular view controller

-(void) PerformGoBackTo:(NSString *)HereWeGo
{
    NSArray *StackArray = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[StackArray objectAtIndex:[self getStatckIndex:HereWeGo]] animated:YES];
}

//This method calculate the view position in self.navigationController.viewControllers array ..

-(int)getStatckIndex:(NSString *)Object
{
    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    for(int i=0 ; i<[arr count] ; i++)
    {
        if([[arr objectAtIndex:i] isKindOfClass:NSClassFromString(Object)])
        {
            return i;
        }
    }
    return 0;
}

-(void)PopViewController:(NSString *)viewControllerName WithAnimation:(NSString *)AnimationType
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:AnimationType];
    
    NSArray *StackArray = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
    [self.navigationController popToViewController:[StackArray objectAtIndex:[self getStatckIndex:viewControllerName]] animated:NO];
}

//POP REALTED METHOD END

//PUSH RELATED METHOD START

-(void)PushViewController:(UIViewController *)viewController WithAnimation:(NSString *)AnimationType
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:AnimationType];
    
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
    [[self navigationController] pushViewController:viewController animated:NO];
}

-(void)PushViewController:(UIViewController *)viewController TransitationFrom:(NSString *)TransitationDirection
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.2f];
    [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:TransitationDirection];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
    [[self navigationController] pushViewController:viewController animated:NO];
}


//Time related methods Start
-(NSString *)LocalDate
{
    NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:[NSDate date]];
}

-(NSString *)LocalTimeZoneName
{
    NSString *TimeZone=[[[NSTimeZone localTimeZone] name] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return [TimeZone stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}


//Changes on view related method Start
-(void)SetBorderToTextFieldView:(UIView *)TxtFieldView Withcolor:(UIColor *)Bordercolour
{
    [[TxtFieldView layer] setBorderColor:[Bordercolour CGColor]];
    [[TxtFieldView layer] setCornerRadius:4.0f];
    [[TxtFieldView layer] setBorderWidth:2.0f];
    [[TxtFieldView layer] setMasksToBounds:YES];
}

-(void)SetBorderToView:(UIView *)Views
{
    [[Views layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[Views layer] setCornerRadius:4.0f];
    [[Views layer] setBorderWidth:2.0f];
    [[Views layer] setMasksToBounds:YES];
}


-(void)SetBorderToTextField:(UITextField *)TxtField Withcolor:(UIColor *)Bordercolour
{
    [[TxtField layer] setBorderColor:[Bordercolour CGColor]];
    [[TxtField layer] setCornerRadius:4.0f];
    [[TxtField layer] setBorderWidth:2.0f];
    [[TxtField layer] setMasksToBounds:YES];
    [TxtField setBorderStyle:UITextBorderStyleNone];
}

-(void)SetCornerToButton:(UIButton *)Button
{
    [[Button layer] setCornerRadius:4.0f];
    [[Button layer] setMasksToBounds:YES];
}

-(void)RoundMyProfilePicture:(UIImageView *)PicView
{
    [self RoundMyProfilePicture:PicView withRadious:PicView.frame.size.height/2.0f];
}


-(void)RoundMyProfilePicture:(UIImageView *)PicView withRadious:(float)Radious
{
    [[PicView layer] setCornerRadius:Radious];
    [[PicView layer] setBorderColor:[[UIColor lightTextColor] CGColor]];
    [[PicView layer] setBorderWidth:2.0f];
    [[PicView layer] setMasksToBounds:YES];
}

-(void)makeRoundWithDropShadow:(UIImageView *)PicView InView:(UIView *)BackView
{
    [self RoundMyProfilePicture:PicView withRadious:[PicView frame].size.width/2.0f];
    
    CGRect tempRect=[PicView frame];
    tempRect.origin.x-=1.0f;
    tempRect.origin.y-=1.0f;
    tempRect.size.height+=2.0f;
    tempRect.size.width+=2.0f;
    
    UIView *VImageHolder=[[UIView alloc] initWithFrame:tempRect];
    [[VImageHolder layer] setShadowColor:[[UIColor lightGrayColor] CGColor]];
    
    [[VImageHolder layer] setMasksToBounds:NO];
    [BackView addSubview:VImageHolder];
    [VImageHolder addSubview:PicView];
    [PicView setFrame:CGRectMake(1.0f, 1.0f, [PicView frame].size.width, [PicView frame].size.height)];
    [BackView bringSubviewToFront:VImageHolder];
}


-(void)setRoundBorderToImageView:(UIImageView *)PicView
{
    [[PicView layer] setCornerRadius:[PicView frame].size.width/2.0f];
    [[PicView layer] setBorderColor:[UIColor clearColor].CGColor];
    [[PicView layer] setBorderWidth:0.0f];
    [[PicView layer] setMasksToBounds:YES];
}


-(void)SetroundborderWithborderWidth:(CGFloat)BorderWidth WithColour:(UIColor *)RGB ForImageview:(UIImageView *)ImageView
{
    
    [[ImageView layer] setCornerRadius:[ImageView frame].size.width/2.0f];
    [[ImageView layer] setBorderColor:[RGB CGColor]];
    [[ImageView layer] setBorderWidth:BorderWidth];
    [[ImageView layer] setMasksToBounds:YES];
    
}

-(void)SetRoundborderToView:(UIView *)Viewname Withcolor:(UIColor *)ColorName WithBorderwith:(CGFloat)boderWidth
{
    [[Viewname layer] setCornerRadius:[Viewname frame].size.width/2.0f];
    [[Viewname layer] setBorderColor:[ColorName CGColor]];
    [[Viewname layer] setBorderWidth:boderWidth];
    [[Viewname layer] setMasksToBounds:YES];
}


-(void)setRoundBorderToUiview:(UIView *)uiview
{
    [[uiview layer] setCornerRadius:[uiview frame].size.width/2.0f];
    [[uiview layer] setBorderColor:[UIColor clearColor].CGColor];
    [[uiview layer] setBorderWidth:0.0f];
    [[uiview layer] setMasksToBounds:YES];
}

-(void)setRoundBorderTolable:(UILabel *)uiviewLabl
{
    [[uiviewLabl layer] setCornerRadius:[uiviewLabl frame].size.width/2.0f];
    [[uiviewLabl layer] setBorderColor:[UIColor clearColor].CGColor];
    [[uiviewLabl layer] setBorderWidth:0.0f];
    [[uiviewLabl layer] setMasksToBounds:YES];
}


-(void)setBorderToView:(UIView *)VictimView withCornerRadius:(float)Radious withColor:(UIColor *)color
{
    [self setBorderToView:VictimView withCornerRadius:Radious withColor:color withBorderWidth:1.0f];
}

-(void)setBorderToView:(UIView *)VictimView withCornerRadius:(float)Radious withColor:(UIColor *)color withBorderWidth:(float)BorderWidth
{
    [[VictimView layer] setCornerRadius:Radious];
    [[VictimView layer] setBorderColor:[color CGColor]];
    [[VictimView layer] setBorderWidth:BorderWidth];
    [[VictimView layer] setMasksToBounds:YES];
}

-(void)setBorderToViews:(NSArray *)ViewArray withCornerRadius:(float)Radious withColor:(UIColor *)color
{
    for(UIView *view in ViewArray) [self setBorderToView:view withCornerRadius:Radious withColor:color withBorderWidth:1.0f];
}

-(void)SetCornerToView:(UIView *)Views
{
    [[Views layer] setCornerRadius:2.0f];
    [[Views layer] setMasksToBounds:YES];
}

-(void)SetCornerToView:(UIView *)Views to:(float)corner
{
    [[Views layer] setCornerRadius:corner];
    [[Views layer] setMasksToBounds:YES];
}

-(void)setRadiousToBox:(UIView *)View to:(float)corner
{
    [[View layer] setCornerRadius:corner];
    [[View layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[View layer] setBorderWidth:1.0f];
    [[View layer] setMasksToBounds:YES];
}

-(void)SetCornerToViews:(NSArray *)Views
{
    for(UIView *sub in Views)
    {
        [self SetCornerToView:sub];
    }
}

-(void)ResignalltextfieldinaView:(UIScrollView *)Scroll
{
    for (UIView *subviews in [Scroll subviews])
    {
        if ([subviews isKindOfClass:[UITextField class]])
        {
            UITextField *txtfield=(UITextField *)subviews;
            [txtfield resignFirstResponder];
        }
        else if ([subviews isKindOfClass:[UITextView class]])
        {
            UITextView *textview=(UITextView *)subviews;
            [textview resignFirstResponder];
        }
        else
        {
            for (UIView *subSubview in [subviews subviews])
            {
                if ([subSubview isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfield=(UITextField *)subSubview;
                    [txtfield resignFirstResponder];
                }
                else if ([subSubview isKindOfClass:[UITextView class]])
                {
                    UITextView *textview=(UITextView *)subSubview;
                    [textview resignFirstResponder];
                }
                
            }
        }
    }
}

-(void)ResignallinaUiviewsunview:(UIView *)Scroll
{
    for (UIView *subviews in [Scroll subviews])
    {
        if ([subviews isKindOfClass:[UITextField class]])
        {
            UITextField *txtfield=(UITextField *)subviews;
            [txtfield resignFirstResponder];
        }
        else if ([subviews isKindOfClass:[UITextView class]])
        {
            UITextView *textview=(UITextView *)subviews;
            [textview resignFirstResponder];
        }
        else
        {
            for (UIView *subSubview in [subviews subviews])
            {
                if ([subSubview isKindOfClass:[UITextField class]])
                {
                    UITextField *txtfield=(UITextField *)subSubview;
                    [txtfield resignFirstResponder];
                }
                else if ([subSubview isKindOfClass:[UITextView class]])
                {
                    UITextView *textview=(UITextView *)subSubview;
                    [textview resignFirstResponder];
                }
                
            }
        }
    }
    
}

-(void)SetBorderToBackView:(UIView *)BackView Withcolor:(UIColor *)Bordercolour
{
    [[BackView layer] setBorderColor:[Bordercolour CGColor]];
    [[BackView layer] setCornerRadius:4.0f];
    [[BackView layer] setBorderWidth:1.0f];
    [[BackView layer] setMasksToBounds:YES];
}

-(void)SetDropShadowToBackView:(UIView *)BackView
{
    [[BackView layer] setMasksToBounds:NO];
    
    [[BackView layer] setShadowOffset:CGSizeMake(2.0f, 2.0f)];
    [[BackView layer] setShadowOpacity:0.2f];
    [[BackView layer] setShouldRasterize:YES];
    [[BackView layer] setRasterizationScale:[UIScreen mainScreen].scale];
}


-(void)SetBorderToFieldIn:(UIView *)FieldView Withcolor:(UIColor *)Color
{
    for(UIView *sub in [FieldView subviews])
    {
        if ([sub isKindOfClass:[UIView class]])
        {
            [sub.layer setBorderColor:Color.CGColor];
            sub.layer.borderWidth=2.0f;
            sub.layer.cornerRadius=2.0f;
            [sub.layer setMasksToBounds:YES];
            
        }
    }
}

-(void)SetDelegetsToTextFiledIn:(UIView *)FieldView
{
    for (UIView *Subview in [FieldView subviews])
    {
        if ([Subview isKindOfClass:[UITextField class]])
        {
            UITextField *textfld=(UITextField *)FieldView;
            textfld.delegate=self;
        }
    }
}


-(void)SetBorderToSubViewOf:(UIView *)Views Withcolor:(UIColor *)Color
{
    for(UIView *sub in [Views subviews])
    {
        if([sub tag]>0)
        {
            [[sub layer] setBorderColor:[Color CGColor]];
            [[sub layer] setBorderWidth:2.0f];
            [[sub layer] setCornerRadius:4.0f];
            [[sub layer] setMasksToBounds:YES];
        }
    }
}

-(void)SetfonToallthetextfielsUnderView:(UIView *)ViewSuper
{
    for (UIView *Subview in [ViewSuper subviews])
    {
        if ([Subview isKindOfClass:[UITextField class]])
        {
            UITextField *textViewe=(UITextField *)Subview;
            //textViewe.font=[UIFont fontWithName:ARMITAREGULAR size:15.0f];
            [textViewe setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
            
        }
    }
}


+(NSString *)GetClassName
{
    return NSStringFromClass([self class]);
}

+(VIGlobalViewController *)sharedViewController
{
    VIGlobalViewController *Objglobal = [MainStory instantiateViewControllerWithIdentifier:[self GetClassName]];
    return Objglobal;
}

-(NSMutableDictionary *)GetkeyandvalueDictionary:(NSArray *)keys values:(NSArray *)Values
{
    NSMutableDictionary *dicmut=[[NSMutableDictionary alloc] init];
    for (NSInteger i=0;i< [keys count];i++)
    {
        [dicmut setValue:[Values objectAtIndex:i] forKey:[keys objectAtIndex:i]];
    }
    return dicmut;
}


-(NSString *)CreatepostString:(NSArray *)Key Withvalues:(NSArray *)values
{
    NSMutableString *URLstring = [[NSMutableString alloc]init];
    for (NSInteger i=0;i<[values count];i++)
    {
        if(i==Key.count-1)
            [URLstring appendString:[NSString stringWithFormat:@"%@=%@",[Key objectAtIndex:i],[values objectAtIndex:i]]];
        else
            [URLstring appendString:[NSString stringWithFormat:@"%@=%@&",[Key objectAtIndex:i],[values objectAtIndex:i]]];
    }
    NSString *FinalString = [NSString stringWithFormat:@"%@",URLstring];
    return FinalString;
}

//getlogin cradencial

-(NSString *)getloginid
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *loginid=[appdel.userDetails valueForKey:@"loginid"];
    return loginid;
    
}

-(NSString *)Getusername
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *loginid=[appdel.userDetails valueForKey:@"loginname"];
    return loginid;
}

-(NSString *)GetPassword
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *loginid=[appdel.userDetails valueForKey:@"loginpassword"];
    return loginid;
}

-(void)Setuserid:(NSString *)userid
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdel.userDetails setValue:userid forKey:@"loginid"];
    [[appdel userDetails] synchronize];
}


-(void)Setusernsme:(NSString *)username
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdel.userDetails setValue:username forKey:@"loginname"];
    [[appdel userDetails] synchronize];
}


-(void)Setuserpassword:(NSString *)userpassord
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdel.userDetails setValue:userpassord forKey:@"loginpassword"];
    [[appdel userDetails] synchronize];
}


-(void)Setsenderimage:(NSString *)Senderimage
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appdel.userDetails setValue:Senderimage forKey:@"senderimage"];
    [[appdel userDetails] synchronize];
}

-(NSString *)Getsenderimage
{
    NSString *Senderimagestring=@"";
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Senderimagestring = [appdel.userDetails valueForKey:@"senderimage"];
    return Senderimagestring;
}

//Spinner for this app
-(void)UserFullname:(NSString *)username
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdel.userDetails setValue:username forKey:@"username"];
    [[appdel userDetails] synchronize];
}

-(NSString *)getusernameset
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *loginid=[appdel.userDetails valueForKey:@"username"];
    return loginid;
}

-(NSString *)GetUserType
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *loginid=[appdel.userDetails valueForKey:@"usertype"];
    return loginid;
}

-(void)Setusertype:(NSString *)usertype
{
    AppDelegate *appdel=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdel.userDetails setValue:usertype forKey:@"usertype"];
    [[appdel userDetails] synchronize];
}

-(void)ShowunexpectedError
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Unexpected error occured." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
