//
//  AppDelegate.h
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Login.h"
@class SWRevealViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(retain,nonatomic)UINavigationController *nav;
@property (strong, nonatomic) SWRevealViewController *viewController;
@property (nonatomic, strong) NSUserDefaults *userDetails;

@end

