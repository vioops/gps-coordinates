//
//  FriendsViewCell.m
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "FriendsViewCell.h"

@implementation FriendsViewCell
@synthesize lblTitle,lblSubTitle;

//- (void)awakeFromNib {
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       // CGRect contentRect = self.contentView.bounds;
        
        self.imgFriend = [[UIImageView alloc]init];
        self.imgFriend.frame = CGRectMake(0, 0, 65, 65);
       
        
        [self.contentView addSubview:self.imgFriend];
        
        
        self.viewForBaselineLayout.layer.masksToBounds = YES;
        self.imgFriend.layer.cornerRadius=CGRectGetWidth(self.imgFriend.frame)/2.0f;
        self.imgFriend.layer.masksToBounds = YES;
        
        lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imgFriend.frame)+10,CGRectGetMinY(self.imgFriend.frame)+15, 200, 18)];
        lblTitle.font=[UIFont fontWithName:@"HelveticaNeue-Light" size:16];
        
        lblSubTitle=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.lblTitle.frame), CGRectGetMaxY(lblTitle.frame)+5, 200, 14)];
        lblSubTitle.font=[UIFont fontWithName:@"HelveticaNeue-Light" size:12];
        lblSubTitle.textColor=[UIColor lightGrayColor];
        
        
        
        
        _btnDecline=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [_btnDecline setBackgroundImage:[UIImage imageNamed:@"close_new"] forState:UIControlStateNormal];
        
        _btnAccept=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [_btnAccept setBackgroundImage:[UIImage imageNamed:@"right"] forState:UIControlStateNormal];
      
        
        
        
        [self.contentView addSubview:_btnAccept];
        [self.contentView addSubview:_btnDecline];
        
        [self.contentView addSubview:lblTitle];
        [self.contentView addSubview:lblSubTitle];
   
        
        
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    //CGFloat boundsX = contentRect.origin.x;
    CGPoint frameCenter;
    CGRect frame;
    
    

    
    frameCenter=CGPointMake(CGRectGetWidth(_imgFriend.frame)/2.0f+10, CGRectGetHeight(contentRect)/2.0f);
    _imgFriend.center=frameCenter;
    
    frame=CGRectMake(CGRectGetMaxX(self.imgFriend.frame)+10,CGRectGetMinY(self.imgFriend.frame)+15, 200, 18);
    lblTitle.frame=frame;
    
    
    frame=CGRectMake(CGRectGetMinX(self.lblTitle.frame), CGRectGetMaxY(lblTitle.frame)+5, 200, 14);
    lblSubTitle.frame=frame;
    
    
    frameCenter=CGPointMake(CGRectGetWidth(contentRect)-25, CGRectGetMidY(_imgFriend.frame));
    _btnDecline.center=frameCenter;

    
    frameCenter=CGPointMake(CGRectGetMinX(self.btnDecline.frame)-25, CGRectGetMidY(_imgFriend.frame));
    _btnAccept.center=frameCenter;
    
}



@end
