//
//  Friends.m
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "VIMainHeader.h"

@interface Friends ()
@property (strong, nonatomic) IBOutlet UIView *mainsearchview;

@end

@implementation Friends

- (void)viewDidLoad
{
    [super viewDidLoad];
    SWRevealViewController *revealController = [self revealViewController];
    
    revealController.navigationController.navigationBarHidden=YES;
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    _tblFriendList.delegate=self;
    _tblFriendList.dataSource=self;
    [_btnSlideMenu addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellID=@"cellID";
    
    FriendsViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil)
    {
        
        cell=[[FriendsViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.imgFriend.image=[UIImage imageNamed:@"user_img"];
    
    cell.lblTitle.text=@"Demo title text";
    cell.lblSubTitle.text=@"Demo subtitle Text";
    
    
    
    if (_segFriendsListReq.selectedSegmentIndex==1) {
        cell.btnAccept.hidden=YES;
        
    }
    else
    {
        cell.btnAccept.hidden=NO;
    }
    return cell;
    
    
}
- (IBAction)HookButton:(id)sender
{
    
    
}
- (IBAction)segFriendsListReqSelection:(UISegmentedControl *)sender {
    
    NSLog(@"%ld",(long)sender.selectedSegmentIndex);
    [_tblFriendList reloadData];
    
}
@end
