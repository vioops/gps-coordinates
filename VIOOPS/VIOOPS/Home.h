//
//  Home.h
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "ViewController.h"
//#import "SWRevealViewController.h"
#import "VIGlobalViewController.h"
@interface Home : VIGlobalViewController<SWRevealViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionGalley;
@property (weak, nonatomic) IBOutlet UIButton *btnSlideMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnListMap;

@end
