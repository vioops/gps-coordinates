//
//  VIGlobalMethods.m
//  VIOOPS
//
//  Created by Rohit on 23/08/15.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import "VIGlobalMethods.h"

@implementation VIGlobalMethods

+(instancetype)Shareinstance
{
    static VIGlobalMethods *GlobalCall;
    static dispatch_once_t dispatchonst;
    dispatch_once(&dispatchonst, ^{
        GlobalCall=[[VIGlobalMethods alloc] init];
    });
    return GlobalCall;
}

-(NSString *) Encoder:(NSString *)str
{
    NSString *trimmedString = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return [trimmedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(NSString *)trim:(NSString *)str
{
    NSString *trimmedString = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return trimmedString;
}


-(BOOL)IsContainAnythingIn:(NSString *)str except:(NSString *)exceptString
{
    if([self IsBlank:str]) return FALSE;
    else if([[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:exceptString]) return FALSE;
    else return TRUE;
}

-(BOOL)IsBlank:(NSString *)str
{
    if([str isEqual:[NSNull null]]) return TRUE;
    return ([[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])?TRUE:FALSE;
}

-(NSData *)ConvertImageToNSData:(UIImage *)image
{
    return UIImageJPEGRepresentation(image, .4);
}

-(NSURL *)ConvertStringToNSUrl:(NSString *)String
{
    return [NSURL URLWithString:String];
}

-(NSString *)ConvertNSDataToNSString:(NSData *)data
{
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}


+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    
    //IMFAPPPRINTMETHOD();
    if(![hexString isEqual:[NSNull null]] && [hexString length]>0)
    {
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    }
    else
        return [UIColor blackColor];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}
@end

