
/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 Original code:
 Copyright (c) 2011, Philip Kluz (Philip.Kluz@zuui.org)
 
*/

#import "Menu.h"

#import "SWRevealViewController.h"
#import "Home.h"
#import "Friends.h"


@interface Menu()
{
    NSInteger _presentedRow;
}

@end

@implementation Menu

@synthesize rearTableView = _rearTableView;


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = NSLocalizedString(@"Menu", nil);
}


#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return 100.0f;
        
    }
    else
        return 45.0f;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
    
   
    

    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIImageView *icon=[[UIImageView alloc]init];
                      
    //icon.backgroundColor=[UIColor grayColor];
    icon.frame=CGRectMake(10, 7, 35, 35);
    [cell addSubview:icon];
	
    NSString *text = nil;
    if (row == 0)
    {
        text = @"";
        icon.frame=CGRectMake(100, 10, 90, 70);
        icon.image=[UIImage imageNamed:@"logored"];
        cell.backgroundColor=[UIColor colorWithRed:219/255.0f green:75/255.0f blue:16/255.0f alpha:1];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        

        
    }
    if (row == 1)
    {
        text = @"Home";
        icon.image=[UIImage imageNamed:@"home"];
    }
    else if (row == 2)
    {
        text = @"My Friends";
        icon.image=[UIImage imageNamed:@"friends"];
    }
    else if (row == 3)
    {
        text = @"Settings";
        icon.image=[UIImage imageNamed:@"settings"];
    }
    else if (row == 4)
    {
        text = @"Share Vioops";
        icon.image=[UIImage imageNamed:@"share"];
    }
    else if (row == 5)
    {
        text = @"Help";
        icon.image=[UIImage imageNamed:@"help"];
    }

    cell.indentationLevel=4;
    
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:8];
    cell.backgroundColor=[UIColor colorWithRed:219/255.0f green:75/255.0f blue:16/255.0f alpha:1];
    
    cell.textLabel.text =text;
    
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // selecting row
    NSInteger row = indexPath.row;
    
    // if we are trying to push the same row or perform an operation that does not imply frontViewController replacement
    // we'll just set position and return
    
    if ( row == _presentedRow )
    {
        [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        return;
    }
//    else if (row == 2)
//    {
//        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
//        return;
//    }
//    else if (row == 3)
//    {
//        [revealController setFrontViewPosition:FrontViewPositionRight animated:YES];
//        return;
//    }

    // otherwise we'll create a new frontViewController and push it with animation

    UIViewController *newFrontController = nil;

    if (row == 0)
    {
        newFrontController = [[Home alloc] init];
    }
    
    else if (row == 1)
    {
        newFrontController = [[Home alloc] init];

        
    }
    else if (row == 2)
    {
        newFrontController=[[Friends alloc]initWithNibName:@"Friends" bundle:nil];
        
    }

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    navigationController.navigationBarHidden=YES;
    if (newFrontController!=nil) {
         [revealController pushFrontViewController:navigationController animated:YES];
    }
   
   
    
    _presentedRow = row;  // <- store the presented row
}



//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}

@end