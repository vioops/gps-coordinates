//
//  Friends.h
//  VIOOPS
//
//  Created by Purnendu mishra on 16/08/2015.
//  Copyright (c) 2015 Purnendu mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VIGlobalViewController.h"
@interface Friends :VIGlobalViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblFriendList;
@property (weak, nonatomic) IBOutlet UIButton *btnSlideMenu;
- (IBAction)segFriendsListReqSelection:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segFriendsListReq;

@end
